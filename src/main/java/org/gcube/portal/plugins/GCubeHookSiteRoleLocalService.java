package org.gcube.portal.plugins;

import java.util.List;

import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.client.dsl.Util;
import org.gcube.common.storagehub.client.dsl.VREFolderManager;
import org.gcube.portal.plugins.util.AuthUtil;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.model.Role;
import com.liferay.portal.model.UserGroupRole;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserGroupRoleLocalService;
import com.liferay.portal.service.UserGroupRoleLocalServiceWrapper;

public class GCubeHookSiteRoleLocalService extends UserGroupRoleLocalServiceWrapper {
	/**
	 * logger
	 */
	private static final Logger _log = LoggerFactory.getLogger(GCubeHookSiteRoleLocalService.class);
	private GroupManager gm;
	public GCubeHookSiteRoleLocalService(UserGroupRoleLocalService userGroupRoleLocalService) {
		super(userGroupRoleLocalService);
		gm = new LiferayGroupManager();
		System.out.println("GCubeHookSiteRoleLocalService hook is UP & Listening ...");
	}

	@Override
	public java.util.List<com.liferay.portal.model.UserGroupRole> addUserGroupRoles(
			long userId, long groupId, long[] roleIds)
					throws com.liferay.portal.kernel.exception.SystemException {			
		List<UserGroupRole> toReturn = super.addUserGroupRoles(userId, groupId,	roleIds);
		try {
			_log.debug("Check if addUserGroupRoles is done in a VRE");
			if (gm.isVRE(groupId)) {
				_log.debug("addUserGroupRoles performed in a VRE, groupId=" + groupId);
				boolean vreManagerRolePresent = false;
				for (int i = 0; i < roleIds.length; i++) {
					Role role = RoleLocalServiceUtil.getRole(roleIds[i]);
					if (role.getName().compareTo(GCubeRole.VRE_MANAGER_LABEL) == 0) { 
						_log.info("User is being promoted (or was) as VREFolder Administrator, userId=" + userId + " on Site groupId="+groupId);
						vreManagerRolePresent = true;
						break;
					}					
				} 
				setVREFolderAdministrator(userId, groupId, vreManagerRolePresent);
			} else {
				_log.debug("addUserGroupRoles NOT done in a VRE, groupId=" + groupId);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return toReturn;
	}

	private void setVREFolderAdministrator(long userId, long groupId, boolean enable) throws Exception {
		String previousToken = SecurityTokenProvider.instance.get();
		String previousUMAToken = AccessTokenProvider.instance.get();
		//this put the necessary auth tokens (legacy e UMA) in ThreadLocal
		try {
			AuthUtil.setInfraManagerLegacyAndUMATokenInThreadLocal();

			String context = gm.getInfrastructureScope(groupId);
			String vreFolderTitle = Util.getVREGroupFromContext(context);
			_log.debug("The vreFolderTitle on which the VREFolder role is being {} is {}", enable, vreFolderTitle);
			StorageHubClient shc = new StorageHubClient();
			VREFolderManager vreFolderManager = shc.getVreFolderManager(vreFolderTitle);

			String theUserToPromoteOrDeclass = new LiferayUserManager().getUserById(userId).getUsername();
			_log.info("The {} is being promoted? {} ", theUserToPromoteOrDeclass, enable);
			if (enable) 
				vreFolderManager.setAdmin(theUserToPromoteOrDeclass);
			else 	
				vreFolderManager.removeAdmin(theUserToPromoteOrDeclass);
			_log.info("*VREFolder-Hook** add/remove role operation complete");
		
		} catch (Exception e) {
			_log.error("An error occurred (or the user was already Admin) when trying to update shub role of userId "+ userId + " to the groupId: " + groupId, e);
			throw e;
		} finally {		
			AccessTokenProvider.instance.set(previousUMAToken);
			SecurityTokenProvider.instance.set(previousToken);
		}
	}
}
