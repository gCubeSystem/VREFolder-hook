package org.gcube.portal.plugins.util;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.portal.oidc.lr62.OIDCUmaUtil;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.gcube.vomanagement.usermanagement.model.GatewayRolesNames;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;

public class AuthUtil {
	private static Log _log = LogFactoryUtil.getLog(AuthUtil.class);
	public AuthUtil() {
	}

	public static void setInfraManagerLegacyAndUMATokenInThreadLocal() throws Exception {
		//get the super user
		_log.info("Getting super user with role " + GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
		//get the super user
		String infraContext = "/"+PortalContext.getConfiguration().getInfrastructureName();
		long rootgroupId = new LiferayGroupManager().getGroupIdFromInfrastructureScope(infraContext);			
		User theAdmin = LiferayUserManager.getRandomUserWithRole(rootgroupId, GatewayRolesNames.INFRASTRUCTURE_MANAGER);
		if (theAdmin == null) {
			_log.error("Cannot add the user to the VRE Folder: there is no user having role " + GatewayRolesNames.INFRASTRUCTURE_MANAGER + " on context: " + infraContext);
		}
		String adminUsername = theAdmin.getScreenName();
		_log.info("Got the super user: " + adminUsername);
		String theAdminToken = PortalContext.getConfiguration().getCurrentUserToken(infraContext, adminUsername);
		List<String> rolesString = new ArrayList<String>();
		List<GCubeRole> theAdminRoles = new LiferayRoleManager().listRolesByUserAndGroup(theAdmin.getUserId(), rootgroupId);			
		for (GCubeRole gCubeRole : theAdminRoles) {
			rolesString.add(gCubeRole.getRoleName());
		}
		rolesString.add(GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
		_log.debug("legacy authorizationService().setTokenRoles done");
		authorizationService().setTokenRoles(theAdminToken, rolesString);
		SecurityTokenProvider.instance.set(theAdminToken);
		OIDCUmaUtil.provideConfiguredPortalClientUMATokenInThreadLocal("/" + PortalContext.getConfiguration().getInfrastructureName());
		_log.debug("new authorizationService PortalClient set UMA-Token done");
	}

}
