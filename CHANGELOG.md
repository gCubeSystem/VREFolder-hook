
# Changelog for VREFolder Liferay Hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.8.4]

- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]

## [v6.8.3] - 2021-11-30

- Bug Fix #22483: setUser2VREFolder may not restore ThreadLocal user variable

## [v6.8.2] - 2021-05-25

- Feature #21505: Updated to support new UMATokensProvider class

## [v6.8.1] - 2021-04-12

Just removed obsolete Home library deps from pom which were forgotten there in 6.8.0 release

## [v6.8.0] - 2021-02-04

Now uses storagehub methods and new auth (UMA tokens) to perform user adding and removal form VRE Folder and Roles (VRE-Manager) to set VRE Folder Administrators


## [v6.7.1] - 2020-11-17

Ported to git

Removed trigger that updates LDAP group upon users add/remove to VREs

## [v6.6.0] - 2018-03-02

Feature #6094 User export to LDAP on create account and join/leave VRE

## [v6.5.0] - 2017-05-23

Feature	#8242 Enhance VRE Folder hook to also create Share LaTeX user on user add to VRE

## [v6.4.0] - 2017-02-23

Feature #6680, added hook to keep in sync VRE Managers and Workspace VRE Folder Managers

## [v6.3.0] - 2016-03-16

First release after switch to new liferay version (6.2.6)
